from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def transaction(request):
    transaction = Receipt.objects.filter(purchaser=request.user)
    context = {
        "transaction": transaction
    }
    return render(request, "receipts/receipt.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category_list,
    }
    return render(request, "receipts/categories.html", context)

@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account_list,
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expenseform = form.save(False)
            expenseform.owner = request.user
            expenseform.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form
    }
    return render(request, "receipts/expense_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            accountform = form.save(False)
            accountform.owner = request.user
            accountform.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create_account.html", context)
