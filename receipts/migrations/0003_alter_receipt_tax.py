# Generated by Django 4.2 on 2023-04-19 22:51

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_receipt_tax_alter_receipt_total"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="tax",
            field=models.DecimalField(decimal_places=3, max_digits=10),
        ),
    ]
