from django.contrib import admin
from receipts.models import Receipt, Account, ExpenseCategory

# Register your models here.
@admin.register(Account)
class Account(admin.ModelAdmin):
    list_display = ("name", "number", "owner")

@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = ("name", "owner")

@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = ("vendor", "total", "date", "category", "purchaser", "account")
